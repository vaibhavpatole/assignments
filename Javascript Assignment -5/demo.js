$(document).ready(function(){
  var value,s,
  switchboard = {
    settings: {
      button: '#switch-btn',
      capture:'#captcha',
      board:'#switchboard',
      checkbox:'#captcha',
      status:'#status',
      message:'#message',
      row:'td',
    },

    init:function(){
      s = this.settings;
      this.bindUIActions();
      this.dragMenu();
    },
    dragMenu:function(){
      $(".table1,.table2").sortable({
         connectWith: '.table2,.table1',
         items: "li:not(:last-child)"
      });
    },

    bindUIActions: function() {
      value=this;
      $(document).on('click', s.button, value.changeBackColor);
      $(document).on('click', s.checkbox,value.changeButtonColor);
    },

    changeButtonColor:function(){
      $(s.button).toggleClass("green");
    },

    changeBackColor:function(){
      value.changeStatus();
      value.changeRowColor();
      $(s.board).toggleClass("gray");
      $(s.message).toggleClass("show");
    },

    changeStatus:function(){
      if($(s.status).html()==='Status :OFF'){
        $(s.status).text("Status :ON");
      }
      else{
        $(s.status).text("Status :OFF");
      }
    },

    changeRowColor:function(){
      $("li:even").toggleClass("gray");
      $("li:odd").toggleClass("black");
    }
  }
  switchboard.init();
});