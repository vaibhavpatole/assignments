CREATE TABLE seat_types (id INTEGER PRIMARY KEY,seat_type VARCHAR(10));
INSERT INTO seat_types (id, seat_type)VALUES (1,'upper'),(2,'lower'),(3,'middle');

CREATE TABLE seat_falilities (id INTEGER PRIMARY KEY,facility VARCHAR(10));
INSERT INTO seat_falilities (id, facility)VALUES (1,'AC'),(2,'Non_AC'),(3,'sleeper');

CREATE TABLE passengers (id INTEGER PRIMARY KEY,name VARCHAR(10),age INTEGER,gender VARCHAR(5),phone_no VARCHAR(10),adhar_details VARCHAR(11));
INSERT INTO passengers (id, name,age,gender,phone_no,adhar_details)VALUES (1,'vaibhav',23,'male','9850301146','12345678901'),(2,'vaibhav',23,'male','9850301146','12345678901'),(3,'vaibhav',23,'male','9850301146','12345678901');

CREATE TABLE counter_workers (id INTEGER PRIMARY KEY,name VARCHAR(10),phone_no VARCHAR(10),adhar_details VARCHAR(11));
INSERT INTO counter_workers(id,name,phone_no,adhar_details) VALUES (1,'vaibhav','9850301146',12345678901),(2,'akash','9850301146',12345678901),(3,'samadhan','9850301146',12345678901);

CREATE TABLE engines (id INTEGER PRIMARY KEY,engine_number INTEGER);
INSERT INTO engines (id,engine_number) VALUES (1,123456),(2,123456),(3,123456);

CREATE TABLE train_types (id INTEGER PRIMARY KEY,type VARCHAR(10));
INSERT INTO train_types (id,type) VALUES (1,'passenger'),(2,'express'),(3,'mail');

CREATE TABLE stations (id INTEGER PRIMARY KEY,name VARCHAR(10),latitude REAL,longitude REAL);
INSERT INTO stations(id,name,latitude,longitude) VALUES (1,'pune',23.45,23.45),(2,'satara',23.45,23.43),(3,'sangali',23.45,78.98);

CREATE TABLE coolies (id INTEGER PRIMARY KEY,name VARCHAR(10),address TEXT,phone_no VARCHAR(10),age INTEGER,adhar_details VARCHAR(11));
INSERT INTO coolies(id, name,address,age,phone_no,adhar_details)VALUES (1,'vaibhav','pune',23,'9850301146','12345678901'),(2,'akash','solapur',33,'9850301146','12345678901'),(3,'samadhan','satara',34,'9850301146','12345678901');

CREATE TABLE shop_owners (id INTEGER PRIMARY KEY,name VARCHAR(10),address TEXT,adhar_details VARCHAR(11));
INSERT INTO shop_owners(id,name,address,adhar_details)VALUES(1,'vaibhav','sangola',12345678901),(2,'vaibhav','sangola',12345678901),(3,'vaibhav','sangola',12345678901);

CREATE TABLE shop_types (id INTEGER PRIMARY KEY,type VARCHAR(10));
INSERT INTO shop_types(id,type)VALUES(1,'food'),(2,'book'),(3,'water'),(4,'toys');

CREATE TABLE counters (id INTEGER PRIMARY KEY,workers_id INTEGER REFERENCES counter_workers(id),stations_id REFERENCES stations(id));
INSERT INTO counters(id,workers_id,stations_id)VALUES(1,1,1),(2,2,2),(3,3,3);

CREATE TABLE vendors (id INTEGER PRIMARY KEY,name VARCHAR(20),owners_id INTEGER REFERENCES shop_owners(id),shop_types_id INTEGER REFERENCES shop_types(id),stations_id INTEGER REFERENCES stations(id));
INSERT INTO vendors(id,name,owners_id,shop_types_id,stations_id)VALUES(1,'jeneral stores',1,1,1),(2,'book stall',2,2,2),(3,'water stall',3,3,3);

CREATE TABLE tracks (id INTEGER PRIMARY KEY,source INTEGER REFERENCES stations(id),destination INTEGER REFERENCES station(id));
INSERT INTO tracks(id,source,destination)VALUES (1,1,2),(2,2,1),(3,1,3),(4,3,1),(5,2,3),(6,3,2);

CREATE TABLE trains (id INTEGER PRIMARY KEY,name VARCHAR(10),engines_id INTEGER REFERENCES engines(id),train_types_id INTEGER REFERENCES train_types(id),source INTEGER REFERENCES stations(id),destination INTEGER REFERENCES stations(id),start_time TIME,end_time TIME);
INSERT INTO trains(id,name,engines_id,train_types_id,source,destination,start_time,end_time)VALUES(1,'Udyan',1,2,1,2,'12:23','23:34'),(2,'zelam',2,3,3,1,'12:23','23:34');

CREATE TABLE station_coolies (id INTEGER PRIMARY KEY,station_id INTEGER REFERENCES stations(id),coolies_id INTEGER REFERENCES coolies(id));
INSERT INTO station_coolies(id,station_id,coolies_id)VALUES(1,2,3),(2,1,1),(3,2,2);

CREATE TABLE platform_tickets (id INTEGER PRIMARY KEY,counters_id INTEGER REFERENCES counters(id),fare REAL);
INSERT INTO platform_tickets(id,counters_id,fare)VALUES(1,1,10),(2,2,20),(3,3,30);

CREATE TABLE tickets (id INTEGER PRIMARY KEY,counters_id INTEGER REFERENCES counters(id),
  passenger_id INTEGER REFERENCES passengers(id),ticket_date date,source INTEGER REFERENCES stations(id),destination INTEGER REFERENCES stations(id),train_id INTEGER REFERENCES trains(id),seat_falicities_id INTEGER REFERENCES seat_falilities(id),seat_types_id INTEGER REFERENCES seat_types(id),seat_number INTEGER,fare REAL);
INSERT INTO tickets(id,counters_id,passenger_id,ticket_date,source,destination,train_id,seat_falicities_id,seat_types_id,seat_number,fare)VALUES(1,1,1,'20200323',1,3,1,2,1,24,500),(2,1,1,'20200312',1,3,1,2,1,25,500);

CREATE TABLE timings (id INTEGER PRIMARY KEY,station_id INTEGER REFERENCES stations(id),trains_id INTEGER REFERENCES trains(id),waiting_time TIME,arival_time TIME,departure_time TIME);
INSERT INTO timings(id,station_id,trains_id,waiting_time,arival_time,departure_time)VALUES(1,2,1,'00:10:30','20:40:00','20:50:30');

CREATE TABLE timing_delays (id INTEGER PRIMARY KEY,station_id INTEGER REFERENCES stations(id),trains_id INTEGER REFERENCES trains(id),platform_no INTEGER,actual_arrival_time TIME,actual_departure_time TIME);
INSERT INTO timing_delays(id,station_id,trains_id,platform_no,actual_arrival_time,actual_departure_time)VALUES (1,2,1,3,'20:43:00','20:53:30');

CREATE TABLE train_locations (id INTEGER PRIMARY KEY,train_id INTEGER REFERENCES trains(id),latitude REAL,longitude REAL);

CREATE TABLE train_routes (id INTEGER PRIMARY KEY,train_id INTEGER REFERENCES trains(id),source INTEGER REFERENCES stations(id),destination INTEGER REFERENCES stations(id),tracks_id INTEGER REFERENCES tracks(id));

CREATE TABLE junctions (id INTEGER PRIMARY KEY,stations_id INTEGER REFERENCES stations(id),platform_no INTEGER,tracks_id INTEGER REFERENCES tracks(id));
