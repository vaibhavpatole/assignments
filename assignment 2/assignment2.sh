# !/bin/sh
echo "Enter FolderName :"
read folderName
echo "Reading File Name :"
mkdir $folderName
echo "Creating Folder"
cd $folderName
echo "Enter No of File you Want to Create :"
read fileNo
for index in $(seq 1 $fileNo)
  do
	 echo $index > $folderName$index.txt
  done
echo "$fileNo files are created "
cd ..
zip $folderName.zip $folderName/*
echo "zip file for $folderName created."
revfolder=$(echo $folderName | rev)
mkdir $revfolder
echo "folder of $revfolder is created"
cd $revfolder
unzip ../$folderName.zip
echo "zip file is unzipped in $revfolder"
cd $folderName
mv * ..
echo "all files from unzipped folder are moved"
cd ..
rmdir $folderName
echo "folder is removed"
for index in $(seq 1 $fileNo)
do
  test=$(( $index % 2 ))
    if [ $test != 0 ];
    then
      chmod 04444 $folderName$index.txt
    fi
done

