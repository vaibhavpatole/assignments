var Students=new Array();
var array=[],total=0,earnedMarks=0,persentage=0;
var Name = document.getElementById("studName");
var Age=document.getElementById("studAge");
var MathsMarks=document.getElementById("studMathsMarks");
var HistoryMarks=document.getElementById("studHistoryMarks");
var BioMarks=document.getElementById("studBioMarks");
var Key=document.getElementById("searchElement");
var display=document.getElementById("displayValue");

function student(name,age,mathMarks,historyMarks,bioMarks){
  var obj = {};
  obj.name = name;
  obj.age=age;
  obj.marks=[parseInt(mathMarks),parseInt(historyMarks),parseInt(bioMarks)];
  return obj;
}
function insertData() {
  var name = Name.value;
  var age=Age.value;
  var mathsMarks=MathsMarks.value;
  var historyMarks=HistoryMarks.value;
  var bioMarks=BioMarks.value;
  if(name.length!=0 && age.length!=0 && mathsMarks.length!=0 && historyMarks!=0 && bioMarks!=0){
    var object=new student(name,age,mathsMarks,historyMarks,bioMarks);
    Students.push(object);
    resetData();
  }
  else{
    alert("Enter all fields");
  }
}
function resetData(){
  Name.value="";
  Age.value="";
  MathsMarks.value="";
  HistoryMarks.value="";
  BioMarks.value="";
}
function displayData(){
  key=Key.value;
  for (var i=0; i < Students.length; i++) {
    if (Students[i].name === key) {
      display.innerHTML="Name :"+Students[i].name+"<br>Age :"+Students[i].age+"<br>Marks :"+Students[i].marks;
    }
  }
}
function displayAge(){
  var key=Key.value;
  for (var i=0; i < Students.length; i++) {
    if (Students[i].name === key) {
      display.innerHTML="Age :"+Students[i].age;
    }
  }
}
function displayPercentage(){
  var i,total=0,devide=0;
  var key=Key.value;
  for (i=0; i < Students.length; i++) {
    if (Students[i].name === key) {
      array=Students[i].marks;
    }
  }
  for(i=0;i<array.length;i++){
    total=total+array[i];
  }
  divide=array.length*100;
  total=(total/divide)*100;
  display.innerHTML="Percentage :"+total;
}