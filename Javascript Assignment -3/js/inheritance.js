Hirarchical Inheritance
var Animal=function(name){
  this.animalName=name;
}
Animal.prototype.getName=function(){
  console.log('name is :'+this.animalName);
};
var Dog=function(name,age){
  Animal.call(this,name);
  this.dogAge=age;
}
Dog.prototype.getAge=function(){
  console.log('age of dog is :'+this.dogAge);
}
var Human=function(name,salary){
  Animal.call(this,name);
  this.salary=salary;
}
Dog.prototype.getSalary=function(){
  console.log('salary of human is :'+this.dogAge);
}
Dog.prototype=Object.create(Animal.prototype);
Human.prototype=Object.create(Animal.prototype);
var labrodog=new Dog("labrodog",23);
var vaibhav=new Human("vaibhav",20000);